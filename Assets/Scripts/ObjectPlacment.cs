﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.VR.WSA.Input;

public class ObjectPlacment : MonoBehaviour {

    GestureRecognizer recognizer;

    private RaycastHit hit;
    private GameObject selectedObj = null;
    private GameObject newClone = null;
    private GameObject currClone = null;
    private AudioSource currAudio = null;

    private List<string> cloneList;
    private int cloneCount = 0;
    private GameObject label;

    // Use this for initialization
    void Start () {

        recognizer = new GestureRecognizer();
        recognizer.SetRecognizableGestures(GestureSettings.Tap);
        recognizer.TappedEvent += Recognizer_TappedEvent;
        recognizer.StartCapturingGestures();
        cloneList = new List<string>();

        label = GameObject.FindGameObjectWithTag("label");  // exception label
        string _text = label.GetComponent<TextMesh>().text; // for debugging
        label.GetComponent<MeshRenderer>().enabled = false; // is invisible
    }

    private void Recognizer_TappedEvent(InteractionSourceKind source, int tapCount, Ray headRay)
    {
        RaycastHit hitInfo;
        Vector3 cPos = Camera.main.transform.position;
        Vector3 cFor = Camera.main.transform.forward;

        // Check if object is being carried
        if (currClone == null)
        {
            if (Physics.Raycast(Camera.main.transform.position, Camera.main.transform.forward, out hitInfo, 20.0f, Physics.DefaultRaycastLayers))
            {
                selectedObj = hitInfo.collider.gameObject; 

                // Determine if the object is a clone
                if (!cloneList.Contains(selectedObj.name.ToString()))
                {
                    cloneCount += 1;
                    Vector3 startpos = Camera.main.transform.position + Camera.main.transform.forward * 1.6f;
                    newClone = Instantiate(selectedObj, startpos, Quaternion.identity) as GameObject;

                    currClone = newClone;
                    currClone.name = selectedObj.name.ToString() + "clone" + cloneCount.ToString();
                    cloneList.Add(currClone.name.ToString());
                    newClone = null;
                }
                else
                {
                    currClone = selectedObj;                                     
                }

                selectedObj = null;
            }  
        }

        else //drop carried object
        {
            currAudio = currClone.GetComponentInChildren<AudioSource>();
            currClone.AddComponent<Rigidbody>();
            currAudio.Play();
            currClone = null;
            currAudio = null;
        }       
    }

    // Update is called once per frame
    void Update () {
        if (currClone != null) {

            currClone.transform.position = Camera.main.transform.position + Camera.main.transform.forward * 1.6f;
            currClone.transform.LookAt(Camera.main.transform.position + Camera.main.transform.rotation * Vector3.back, Camera.main.transform.rotation * Vector3.up);
        }
    }
}
